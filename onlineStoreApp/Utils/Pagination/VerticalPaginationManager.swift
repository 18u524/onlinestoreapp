//
//  VerticalPaginationManager.swift
//  onlineStoreApp
//
//  Created by Кирилл Титов on 24.08.2020.
//  Copyright © 2020 Кирилл Титов. All rights reserved.
//

import UIKit

protocol VerticalPagiationManagerDelegate: class {
	func refreshAll(completion: @escaping (Bool) -> Void)
	func loadMore(completion: @escaping (Bool) -> Void)
}

class VerticalPagiationManager: NSObject {
	private var isLoading = false
	private var isObservingKeyPath = false
	private var scrollView: UIScrollView!
	private var topMostLoader: UIView?
    private var bottomMostLoader: UIView?
	var refreshViewColor: UIColor = .white
	var loaderColor: UIColor = .white

	weak var delegate: VerticalPagiationManagerDelegate?

	init(scrollView: UIScrollView) {
        super.init()
        self.scrollView = scrollView
        self.addScrollViewOffsetObserver()
    }

	deinit {
		self.removeScrollViewOffsetObserver()
	}

	func initialLoad() {
		self.delegate?.refreshAll(completion: { _ in })
	}
}

// MARK: ADD TOP LOADER
extension VerticalPagiationManager {
	func addTopMostControl() {
		let view = UIView()
		view.backgroundColor = self.refreshViewColor
		view.frame.origin = CGPoint(x: 0,
									y: -60)
		view.frame.size = CGSize(width: self.scrollView.bounds.width,
								 height: 60)
		let activity = UIActivityIndicatorView(style: .medium)
		activity.color = self.loaderColor
		activity.frame = view.bounds
		activity.startAnimating()
		view.addSubview(activity)
		self.scrollView.contentInset.top = view.frame.height
		self.topMostLoader = view
		self.scrollView.addSubview(view)
	}

	func removeTopLoader() {
		self.topMostLoader?.removeFromSuperview()
		self.topMostLoader = nil
		self.scrollView.contentInset.top = 0
		self.scrollView.setContentOffset(.zero, animated: true)
	}
}

// MARK: ADD BOTTOM LOADER
extension VerticalPagiationManager {
	func addBottomControl() {
		let view = UIView()
		view.backgroundColor = self.refreshViewColor
		view.frame.origin = CGPoint(x: 0,
									y: self.scrollView.contentSize.height)
		view.frame.size = CGSize(width: self.scrollView.bounds.width,
								 height: 60)
		let activity = UIActivityIndicatorView(style: .medium)
		activity.color = self.loaderColor
		activity.frame = view.bounds
		activity.startAnimating()
		view.addSubview(activity)
		self.scrollView.contentInset.bottom = view.frame.height
        self.bottomMostLoader = view
		self.scrollView.addSubview(view)
	}
	func removeBotomLoader() {
        self.bottomMostLoader?.removeFromSuperview()
        self.bottomMostLoader = nil
    }
}

// MARK: OFFSET OBSERVER
extension VerticalPagiationManager {
	private func addScrollViewOffsetObserver() {
			if self.isObservingKeyPath { return }
			self.scrollView.addObserver(
				self,
				forKeyPath: "contentOffset",
				options: [.new],
				context: nil
			)
			self.isObservingKeyPath = true
	}

	private func removeScrollViewOffsetObserver() {
		if self.isObservingKeyPath {
			self.scrollView.removeObserver(self,
										   forKeyPath: "contentOffset")
		}
		self.isObservingKeyPath = false
	}

	override public func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
		guard let object = object as? UIScrollView,
			let keyPath = keyPath,
			let newValue = change?[.newKey] as? CGPoint,
			object == self.scrollView, keyPath == "contentOffset" else { return }
		self.setContentOffSet(newValue)
	}

	private func setContentOffSet(_ offset: CGPoint) {
		let offsetY = offset.y
		if offsetY < -100 && !self.isLoading {
			self.isLoading = true
			self.addTopMostControl()
			self.delegate?.refreshAll { _ in
				self.isLoading = false
				self.removeTopLoader()
			}
			return
		}

		let contentHeight = self.scrollView.contentSize.height
		let frameHeight = self.scrollView.bounds.size.height
		let diffY = contentHeight - frameHeight
		if contentHeight > frameHeight,
		offsetY > (diffY + 130) && !self.isLoading {
			self.isLoading = true
			self.addBottomControl()
			self.delegate?.loadMore { _ in
				self.isLoading = false
				self.removeBotomLoader()
			}
		}
	}
}
