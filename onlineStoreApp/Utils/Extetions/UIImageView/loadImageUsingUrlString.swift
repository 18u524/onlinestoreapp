//
//  loadImageUsingUrlString.swift
//  onlineStoreApp
//
//  Created by Кирилл Титов on 25.08.2020.
//  Copyright © 2020 Кирилл Титов. All rights reserved.
//

import UIKit
import Alamofire

let imageCache = NSCache<NSString, AnyObject>()
extension UIImageView {

	func loadImageUsingUrlString(urlString: String) {
		image = nil

		// cache version of image
		if let imageFromCache = imageCache.object(forKey: urlString as NSString ) as? UIImage {
			self.image = imageFromCache
			return
		}

		// image into cache
		AF.download(urlString).responseData { response in
			switch response.result {
			case .success:
				let imageToCach = UIImage(data: response.value!)
				DispatchQueue.main.async {
					imageCache.setObject(imageToCach!, forKey: urlString as NSString)
					self.image = imageToCach
				}
			case let .failure(error):
				print(error)
			}
		}.resume()
	}
}
