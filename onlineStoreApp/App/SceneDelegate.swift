//
//  SceneDelegate.swift
//  onlineStoreApp
//
//  Created by Кирилл Титов on 17.04.2020.
//  Copyright © 2020 Кирилл Титов. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(
		_ scene: UIScene,
		willConnectTo session: UISceneSession,
		options connectionOptions: UIScene.ConnectionOptions) {

		let window = UIWindow(windowScene: UIWindowScene(session: session, connectionOptions: connectionOptions))

		let viewC = UIStoryboard(name: "Main", bundle: nil)
			.instantiateViewController(identifier: "authID") as AuthorizationViewController
		viewC.configurator = AuthorizationModuleConfigurator()
		let navC = UINavigationController(rootViewController: viewC)
		window.rootViewController = navC

		self.window = window
		self.window?.makeKeyAndVisible()
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
//        guard let _ = (scene as? UIWindowScene) else { return }
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.

        // Save changes in the application's managed object context when the application transitions to the background.
        (UIApplication.shared.delegate as? AppDelegate)?.saveContext()
    }

}
