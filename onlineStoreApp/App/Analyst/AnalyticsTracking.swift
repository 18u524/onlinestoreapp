//
//  AnalyticsTracking.swift
//  onlineStoreApp
//
//  Created by Kirill Titov on 22.09.2020.
//  Copyright © 2020 Кирилл Титов. All rights reserved.
//

import Foundation
import Firebase

protocol AnalyticEvent: AnalyticsTracking {
	var method: String { get set }
	var parameters: [String : Any] { get set }
}

protocol AnalyticsTracking {
	func track()
}

extension AnalyticEvent {
	func track() {
		Analytics.logEvent(method,
						   parameters: parameters)
	}
}
