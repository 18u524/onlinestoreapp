//
//  LoginFailure.swift
//  onlineStoreApp
//
//  Created by Kirill Titov on 22.09.2020.
//  Copyright © 2020 Кирилл Титов. All rights reserved.
//

import Foundation

struct LoginFailureEvent: AnalyticEvent {
	var method: String = "Login failure"

	var parameters: [String : Any] = ["date" : Date().timeIntervalSince1970]
}
