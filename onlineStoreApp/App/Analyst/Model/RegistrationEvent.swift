//
//  Registration.swift
//  onlineStoreApp
//
//  Created by Kirill Titov on 22.09.2020.
//  Copyright © 2020 Кирилл Титов. All rights reserved.
//

import Foundation

struct RegistrationEvent: AnalyticEvent {
	var method: String = "Registration showed"

	var parameters: [String : Any] = ["date" : Date().timeIntervalSince1970]
}
