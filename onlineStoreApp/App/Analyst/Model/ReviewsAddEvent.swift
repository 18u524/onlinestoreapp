//
//  ReviewsAddEvent.swift
//  onlineStoreApp
//
//  Created by Kirill Titov on 22.09.2020.
//  Copyright © 2020 Кирилл Титов. All rights reserved.
//

import Foundation
struct ReviewsAddEvent: AnalyticEvent {
	var method: String = "ReviewAdded"

	var parameters: [String : Any] = ["date" : Date().timeIntervalSince1970]
}
