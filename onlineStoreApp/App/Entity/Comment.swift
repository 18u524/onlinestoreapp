//
//  Comment.swift
//  onlineStoreApp
//
//  Created by Kirill Titov on 17.09.2020.
//  Copyright © 2020 Кирилл Титов. All rights reserved.
//

import Foundation

class Comment: Codable {
	var avatar: String?
	var userNameAndLastname: String
	var dateComment: String

	var textComment: String

	enum CodingKeys: String, CodingKey {
		case avatar = "1"
		case userNameAndLastname = "2"
		case dateComment = "3"
		case textComment = "4"
	}

	init(avatar: String?, nameLastname: String, date: String, text: String) {
		self.avatar = avatar
		self.userNameAndLastname = nameLastname
		self.dateComment = date
		self.textComment = text
	}
}
