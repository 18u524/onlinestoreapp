//
//  UserInfoUpdateResponse.swift
//  onlineStoreApp
//
//  Created by Кирилл Титов on 02.09.2020.
//  Copyright © 2020 Кирилл Титов. All rights reserved.
//

import Foundation

class UserInfoUpdateUsernameResponse: Codable {}

class UserInfoUpdatePasswordResponse: Codable {}

class UserInfoUpdateAvatarResponse: Codable {}
