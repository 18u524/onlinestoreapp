//
//  RegisterResult.swift
//  onlineStoreApp
//
//  Created by Кирилл Титов on 10.08.2020.
//  Copyright © 2020 Кирилл Титов. All rights reserved.
//

import Foundation

class RegisterResult: Codable {
	let error: Bool
	let reason: String?

	let accessToken: String?
	let refreshToken: String?

	init(error: Bool, reason: String, accessToken: String, refreshToken: String) {
		self.error = error
		self.reason = reason

		self.accessToken = accessToken
 		self.refreshToken = refreshToken
	}
}
