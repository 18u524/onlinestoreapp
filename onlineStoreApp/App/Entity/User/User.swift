//
//  User.swift
//  onlineStoreApp
//
//  Created by Кирилл Титов on 18.04.2020.
//  Copyright © 2020 Кирилл Титов. All rights reserved.
//

import Foundation

//class User: Codable {
//	let id: Int
//	let username: String
//	let email: String
//	let avatar: Data?
//
//	enum CodingKeys: String, CodingKey {
//		case id = "user_id"
//		case username = "username"
//		case email = "email"
//		case avatar = "avatar"
//	}
//
//	init(username: String, email: String, avatar: Data) {
//		self.id = id
//		self.username = username
//		self.email = email
//		self.avatar = avatar
//	}
//}

class User: Codable {

	var username: String = ""
	var email: String = ""
	var avatar: String? = String()

    static var shared: User = {
        let instance = User()

        return instance
    }()

    private init() {}

}
