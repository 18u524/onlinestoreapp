//
//  LoginResult.swift
//  onlineStoreApp
//
//  Created by Кирилл Титов on 18.04.2020.
//  Copyright © 2020 Кирилл Титов. All rights reserved.
//

import Foundation

class AuthResult: Codable {
    let error: Bool?
    let reason: String?

	let accessToken: String?
	let refreshToken: String?
	let expires: Int?

	init(error: Bool, reason: String, accessToken: String, refreshToken: String, expires: Int) {
        self.error = error
        self.reason = reason

		self.accessToken = accessToken
		self.refreshToken = refreshToken
		self.expires = expires
    }
}
