//
//  Product.swift
//  onlineStoreApp
//
//  Created by Кирилл Титов on 13.08.2020.
//  Copyright © 2020 Кирилл Титов. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class Product: Codable {

	var categoryId: Int
	var id: Int
	var name: String
	var cost: Int
	var imageStringUrl: String

	var image: UIImage?

	enum CodingKeys: String, CodingKey {
		case categoryId = "category_id"
		case id = "id"
		case name = "name"
		case cost = "price"
		case imageStringUrl = "image"
	}

	init(categoryId: Int, id: Int, name: String, cost: Int, imageStringUrl: String) {
		self.categoryId = categoryId
		self.id = id
		self.name = name
		self.cost = cost
		self.imageStringUrl = imageStringUrl
	}
}
