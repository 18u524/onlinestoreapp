//
//  Зкщвгсеы.swift
//  onlineStoreApp
//
//  Created by Кирилл Титов on 26.08.2020.
//  Copyright © 2020 Кирилл Титов. All rights reserved.
//

import Foundation

class Products: Codable {
	let count: Int
	let products: [Product]

//	enum CodingKeys: String, CodingKey {
//		case count = "count"
//		case products = "products"
//	}

	init(count: Int, products: [Product]) {
		self.count = count
		self.products = products
	}
}
