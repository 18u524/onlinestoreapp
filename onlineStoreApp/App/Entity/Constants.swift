//
//  Constants.swift
//  onlineStoreApp
//
//  Created by Кирилл Титов on 02.09.2020.
//  Copyright © 2020 Кирилл Титов. All rights reserved.
//

import Foundation

final class Constants {
	final class Path {
		static let userInfoUpdateUsernameStringUrl = ""
		static let userInfoUpdatePasswordStringUrl = ""
		static let userInfoUpdateAvatarStringUrl = ""

		static let fetchCommentStringURL = ""
		static let sendCommentStringURL = ""

		static let fetchProductsStringURL = ""
	}
}
