//
//  ProductListProductListViewInput.swift
//  onlineStoreApp
//
//  Created by kirill on 12/05/2020.
//  Copyright © 2020 none. All rights reserved.
//

protocol ProductListViewInput: class {

    /**
        @author kirill
        Setup initial state of the view
    */
	func reloadData()
    func setupInitialState()
}
