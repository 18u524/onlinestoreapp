//
//  ProductListProductListViewController.swift
//  onlineStoreApp
//
//  Created by kirill on 12/05/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit

private func delay(_ delay: Double, closure: @escaping () -> Void) {
    let deadline = DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
    DispatchQueue.main.asyncAfter(
        deadline: deadline,
        execute: closure
    )
}

class ProductListViewController: UIViewController, ProductListViewInput {

	var output: ProductListViewOutput!

	private lazy var paginationManager: VerticalPagiationManager = {
		let manager = VerticalPagiationManager(scrollView: self.collectionView)
		manager.delegate = self
		return manager
	}()

	private var isDragging: Bool {
		return self.collectionView.isDragging
	}
//	var productList: [Product] = []

	/// collection view
	enum CellID: String {
		case collection = "productCollectionCellID"
	}

	@IBOutlet weak var collectionView: UICollectionView!

	@IBAction func userInfoButtonPressed(_ sender: Any) {
		output.userInfoButtonPressed()
	}

	@IBAction func shoppingBusketPressed() {
		output.shoppingBusketPressed()
	}

    /// Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

		self.setupInitialState()
		self.setupNavController()

		self.setupCollectionView()
        self.setupPagination()
        self.fetchItems()

		ProductListOpenedEvent().track()

        output.viewIsReady()
    }

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)

		self.setupNavController()
	}

    /// ProductListViewInput
    func setupInitialState() {

    }

	func setupNavController() {
		self.navigationController?.setNavigationBarHidden(true, animated: false)
	}

	private func setupCollectionView() {
		collectionView.delegate = self
		collectionView.dataSource = self

		self.regCollectionCell()

		collectionView.alwaysBounceHorizontal = true
	}
}

// MARK: - COLLECTION VIEW
extension ProductListViewController: UICollectionViewDelegate, UICollectionViewDataSource {
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return output.productsCount
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

		guard let cell = collectionView
			.dequeueReusableCell(withReuseIdentifier: CellID.collection.rawValue,
								 for: indexPath) as? ProductCollectionCell else {
			let cell = ProductCollectionCell()
			return cell
		}

		guard let product = output.product(atIndex: indexPath) else { return UICollectionViewCell() }

		cell.setupCell(product: product)

		return cell
	}

	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		output.product(selectAt: indexPath)
	}

	func regCollectionCell() {
		let nib = UINib.init(nibName: "ProductCollectionCell", bundle: nil)
		self.collectionView.register(nib, forCellWithReuseIdentifier: CellID.collection.rawValue)
	}

	public func reloadData() {
		self.collectionView.reloadData()
	}

}

// MARK: PAGINATION
extension ProductListViewController: VerticalPagiationManagerDelegate {

    private func setupPagination() {
		self.paginationManager.refreshViewColor = .clear
		self.paginationManager.loaderColor = .black
    }

    private func fetchItems() {
        self.paginationManager.initialLoad()
    }

    internal func refreshAll(completion: @escaping (Bool) -> Void) {
		delay(0.8) {
			self.output.refreshAll()
			completion(true)
		}
    }

    internal func loadMore(completion: @escaping (Bool) -> Void) {
		delay(0.8) {
			self.output.loadMore()
            completion(true)
        }
    }
}
