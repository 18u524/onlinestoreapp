//
//  ProductListProductListViewOutput.swift
//  onlineStoreApp
//
//  Created by kirill on 12/05/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit

protocol ProductListViewOutput {

    /**
        @author kirill
        Notify presenter that view is ready
    */

	var productsCount: Int { get }
	func product(atIndex indexPath: IndexPath) -> Product?
	func product(selectAt indexPath: IndexPath)

	func refreshAll()
	func loadMore()

    func viewIsReady()

	func userInfoButtonPressed()
	func shoppingBusketPressed()
}
