//
//  ProductListProductListPresenter.swift
//  onlineStoreApp
//
//  Created by kirill on 12/05/2020.
//  Copyright © 2020 none. All rights reserved.
//
import UIKit

class ProductListPresenter: ProductListModuleInput, ProductListViewOutput, ProductListInteractorOutput {

    weak var view: ProductListViewInput!
    var interactor: ProductListInteractorInput!
    var router: ProductListRouterInput!

	private var page = ProductPage()
	private func resetPage() {
		self.page.page = 0
	}
	private func addPage() {
		self.page.page += 1
	}

	private func subPage() {
		self.page.page -= 1
	}

	private var products: [Product] = []
	var productsCount: Int {
		products.count
	}
	private func loadProducts(completion: @escaping ([Product]) -> Void ) {
		interactor.loadProductReq(page: page) { products in
			completion(products?.products ?? [])
		}
	}

	public func product(atIndex indexPath: IndexPath) -> Product? {
		if products.indices.contains(indexPath.row) {
			return products[indexPath.row]
		} else {
			return nil
		}
	}

	public func product(selectAt indexPath: IndexPath) {
		router.goToProduct(withProduct: products[indexPath.row])
	}

	public func refreshAll() {
		self.resetPage()
		self.loadProducts { [weak self] products in
			guard let self = self else { return }
			DispatchQueue.main.async {

				if products.isEmpty {
					self.subPage()
				}

				self.products = products
				self.view.reloadData()
			}
		}
	}

	public func loadMore() {
		self.addPage()
		self.loadProducts { [weak self] products in
			guard let self = self else { return }
			DispatchQueue.main.async {

				if products.isEmpty {
					self.subPage()
				}
//				_=self.products.popLast()
				self.products.append(contentsOf: products)
				self.view.reloadData()
			}
		}
	}

	public func userInfoButtonPressed() {
		router.goToUserInfo()
	}
	
	func shoppingBusketPressed() {
		router.goToShoppingBusket()
	}

    func viewIsReady() {

    }
}
