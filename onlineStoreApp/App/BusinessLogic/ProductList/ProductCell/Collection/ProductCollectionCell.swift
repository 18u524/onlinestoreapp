//
//  ProductCell.swift
//  onlineStoreApp
//
//  Created by Кирилл Титов on 12.08.2020.
//  Copyright © 2020 Кирилл Титов. All rights reserved.
//

import UIKit

class ProductCollectionCell: UICollectionViewCell {

	@IBOutlet weak var productName: UILabel!
	@IBOutlet weak var productCost: UILabel!
	@IBOutlet weak var productImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()

		self.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }

	func setupCell(product: Product) {
		setProductName(name: product.name)
		setProductCost(cost: product.cost)
		setProductImage(path: product.imageStringUrl)
	}

	private func setProductName(name: String) {
		productName.text = name
	}

	private func setProductCost(cost: Int) {
		productCost.text = String(cost)
	}

	private func setProductImage(path: String) {
		productImage.loadImageUsingUrlString(urlString: path)
	}

}
