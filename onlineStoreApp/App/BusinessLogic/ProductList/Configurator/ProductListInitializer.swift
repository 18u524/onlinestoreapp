//
//  ProductListProductListInitializer.swift
//  onlineStoreApp
//
//  Created by kirill on 12/05/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit

class ProductListModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var productlistViewController: ProductListViewController!

    override func awakeFromNib() {

        let configurator = ProductListModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: productlistViewController)
    }

}
