//
//  ProductListProductListConfigurator.swift
//  onlineStoreApp
//
//  Created by kirill on 12/05/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit

class ProductListModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? ProductListViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: ProductListViewController) {

        let router = ProductListRouter()
		router.view = viewController

        let presenter = ProductListPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = ProductListInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
