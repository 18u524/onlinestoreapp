//
//  ProductListProductListRouterInput.swift
//  onlineStoreApp
//
//  Created by kirill on 12/05/2020.
//  Copyright © 2020 none. All rights reserved.
//

import Foundation

protocol ProductListRouterInput {
	func goToUserInfo()
	func goToProduct(withProduct product: Product)
	func goToShoppingBusket()
}
