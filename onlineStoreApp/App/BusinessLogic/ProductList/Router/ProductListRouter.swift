//
//  ProductListProductListRouter.swift
//  onlineStoreApp
//
//  Created by kirill on 12/05/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit

class ProductListRouter: ProductListRouterInput {
	weak var view: ProductListViewController?

	private let userInfoID = "userInfoID"

	func goToUserInfo() {
		let nav = view?.navigationController

		guard let userInfoVC = UIStoryboard
			.init(name: "UserInfo", bundle: nil)
			.instantiateViewController(identifier: userInfoID) as? UserInfoViewController else { return }

		nav?.pushViewController(userInfoVC, animated: true)
	}

	func goToProduct(withProduct product: Product) {
		let viewController = UIStoryboard(name: "Product", bundle: nil).instantiateInitialViewController()

		guard let productVC = viewController as? ProductViewController else { return }
		productVC.output.product = product
		view?.navigationController?.pushViewController(productVC, animated: false)
	}

	func goToShoppingBusket() {
		let viewController = UIStoryboard(name: "ShoppingBasket", bundle: nil).instantiateInitialViewController()

		guard let shoppingBusketVC = viewController as? ShoppingBasketViewController else { return }
		view?.navigationController?.pushViewController(shoppingBusketVC, animated: false)
	}
}
