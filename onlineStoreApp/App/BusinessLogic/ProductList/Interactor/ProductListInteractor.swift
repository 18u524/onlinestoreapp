//
//  ProductListProductListInteractor.swift
//  onlineStoreApp
//
//  Created by kirill on 12/05/2020.
//  Copyright © 2020 none. All rights reserved.
//

import Foundation

class ProductListInteractor: ProductListInteractorInput {

    weak var output: ProductListInteractorOutput!

	func loadProductReq(page: ProductPage, completion: @escaping (Products?) -> Void) {
		ProductLoadService.request(page: page) { response in
			completion(response)
		}
	}
}
