//
//  ProductListProductListInteractorInput.swift
//  onlineStoreApp
//
//  Created by kirill on 12/05/2020.
//  Copyright © 2020 none. All rights reserved.
//

import Foundation

protocol ProductListInteractorInput {
	func loadProductReq(page: ProductPage, completion: @escaping (Products?) -> Void )
}
