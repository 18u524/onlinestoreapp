//
//  ProductLoadService.swift
//  onlineStoreApp
//
//  Created by Кирилл Титов on 24.08.2020.
//  Copyright © 2020 Кирилл Титов. All rights reserved.
//

import Alamofire

final class ProductLoadService {

	class func request(page: ProductPage, completion: @escaping (Products?) -> Void ) {
		let url = "http://localhost:8080/product/get_products"
		let parameters: [String : Int] = ["page" : page.page,
										  "limit" : page.limit]

		AF.request(url, method: .post, parameters: parameters)
			.responseDecodable(of: Products.self) { response in
				switch response.result {
				case let .success(result):
					completion(result)
					return
				case let .failure(error):
					print(error)
					return
				}
		}
	}
}
