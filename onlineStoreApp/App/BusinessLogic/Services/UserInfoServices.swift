//
//  UserInfoServices.swift
//  onlineStoreApp
//
//  Created by Кирилл Титов on 02.09.2020.
//  Copyright © 2020 Кирилл Титов. All rights reserved.
//

import Foundation
import Alamofire

enum UserInfoError: Error {
	case incorrectPassword

}

protocol UserInfoServiceProtocol: class {
	func update(
	Username name: String,
	completion: @escaping (UserInfoUpdateUsernameResponse?) -> Void )

	func update(
	Password newPassword: String,
	oldPassword: String,
	completion: @escaping (UserInfoUpdatePasswordResponse?) -> Void )

	func update(
	Avatar data: Data,
	completion: @escaping (UserInfoUpdateAvatarResponse?) -> Void )
}

class UserInfoServices: UserInfoServiceProtocol {
	func update(
		Username name: String,
		completion: @escaping (UserInfoUpdateUsernameResponse?) -> Void ) {

		let url = Constants.Path.userInfoUpdateUsernameStringUrl
		let parameters: [String : String] = ["" : ""]
		AF.request(url, method: .post, parameters: parameters)
		.responseDecodable(of: UserInfoUpdateUsernameResponse.self) { response in
			switch response.result {
			case let .success(result):
				completion(result)
			case let .failure(error):
				print(error)
			}
		}
	}

	func update(
		Password newPassword: String,
		oldPassword: String,
		completion: @escaping (UserInfoUpdatePasswordResponse?) -> Void ) {

		let url = Constants.Path.userInfoUpdatePasswordStringUrl
		let parameters: [String : String] = ["" : ""]
		AF.request(url, method: .post, parameters: parameters)
		.responseDecodable(of: UserInfoUpdatePasswordResponse.self) { response in
			switch response.result {
			case let .success(result):
				completion(result)
			case let .failure(error):
				print(error)
			}
		}
	}

	func update(
		Avatar data: Data,
		completion: @escaping (UserInfoUpdateAvatarResponse?) -> Void ) {

		let url = Constants.Path.userInfoUpdateAvatarStringUrl
		let parameters: [String : String] = ["" : ""]
		AF.request(url, method: .post, parameters: parameters)
		.responseDecodable(of: UserInfoUpdateAvatarResponse.self) { response in
			switch response.result {
			case let .success(result):
				completion(result)
			case let .failure(error):
				print(error)
			}
		}
	}
}
