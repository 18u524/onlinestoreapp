//
//  ShoppingBasketService.swift
//  onlineStoreApp
//
//  Created by Kirill Titov on 21.09.2020.
//  Copyright © 2020 Кирилл Титов. All rights reserved.
//

import Alamofire

protocol ShoppingBasketServiceProtocol {
	func fetchProducts(completion: @escaping ([Product]) -> Void)
}

class ShoppingBasketService: ShoppingBasketServiceProtocol {

	func fetchProducts(completion: @escaping ([Product]) -> Void) {
		let userEmail = User.shared.email
		let url = Constants.Path.fetchProductsStringURL

		let parameters: [String : String] = ["" : "",
											 "" : ""]

		AF.request(url, method: .post, parameters: parameters)
			.responseDecodable(of: Products.self) { response in
				switch response.result {
				case let .success(products):
					completion(products.products)
				case let .failure(error):
					print(error)
				}
		}
	}
}
