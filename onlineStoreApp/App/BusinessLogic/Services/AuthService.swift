//
//  AuthService.swift
//  onlineStoreApp
//
//  Created by Кирилл Титов on 11.08.2020.
//  Copyright © 2020 Кирилл Титов. All rights reserved.
//

import Foundation
import Alamofire

final class AuthService {
	let url = "http://localhost:8080/auth/login"

	func request(login: String, password: String) {
		let parameters: [String : String] = ["username" : login,
											 "password" : password]

		AF.request(url, method: .post, parameters: parameters)
			.responseDecodable(of: AuthResult.self) { response in
				switch response.result {
				case let .success(authResult):
					NotificationCenter.default.post(name: NSNotification.Name("authCallBack"), object: authResult)
				case let .failure(error):
					print(error)
				}
				NotificationCenter.default.removeObserver("authCallBack")
		}
	}
}
