//
//  CommentsService.swift
//  onlineStoreApp
//
//  Created by Kirill Titov on 17.09.2020.
//  Copyright © 2020 Кирилл Титов. All rights reserved.
//

import Alamofire

protocol CommentsServiceProtocol {
	func fetchComments(
		ForProductId productId: Int,
		completion: @escaping (Comment?) -> Void )

	func sendComment(
		comment: Comment,
		completion: @escaping (Comment) -> Void )
}

class CommentsService: CommentsServiceProtocol {

	func fetchComments(
		ForProductId productId: Int,
		completion: @escaping (Comment?) -> Void) {

		let url = Constants.Path.fetchCommentStringURL
		let parameters: [String : String] = ["" : ""]
		AF.request(url, method: .post, parameters: parameters)
		.responseDecodable(of: Comment.self) { response in
			switch response.result {
			case let .success(result):
				completion(result)
			case let .failure(error):
				print(error)
			}
		}
	}

	func sendComment(comment: Comment, completion: @escaping (Comment) -> Void) {
		let url = Constants.Path.sendCommentStringURL
		let parameters: [String : String] = ["" : ""]
		AF.request(url, method: .post, parameters: parameters)
		.responseDecodable(of: Comment.self) { response in
			switch response.result {
			case let .success(comment):
				print("✅ 'sendComment()' comment is send")
				completion(comment)
			case let .failure(error):
				print(error)
			}
		}
	}
}
