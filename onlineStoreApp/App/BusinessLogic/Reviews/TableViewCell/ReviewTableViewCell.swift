//
//  ReviewTableCellTableViewCell.swift
//  onlineStoreApp
//
//  Created by Kirill Titov on 17.09.2020.
//  Copyright © 2020 Кирилл Титов. All rights reserved.
//

import UIKit

class ReviewTableViewCell: UITableViewCell {

	@IBOutlet weak var avatarImageView: UIImageView!
	@IBOutlet weak var userNameAndLastnameLabel: UILabel!
	@IBOutlet weak var dateCommentLabel: UILabel!

	@IBOutlet weak var textCommentLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

	func setup(Cell avatar: String, user: String, date: String, text: String) {
		self.setup(Avatar: avatar)
		self.setup(User: user)
		self.setup(Date: date)
		self.setup(Comment: text)
	}

	private func setup(Avatar avatar: String) {
		self.avatarImageView.loadImageUsingUrlString(urlString: avatar)
	}
	private func setup(User user: String) {
		self.userNameAndLastnameLabel.text = user
	}
	private func setup(Date dateCommentString: String) {
		self.dateCommentLabel.text = dateCommentString
	}
	private func setup(Comment textCommentString: String) {
		self.textCommentLabel.text = textCommentString
	}
}
