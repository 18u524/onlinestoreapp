//
//  ReviewsReviewsInteractor.swift
//  onlineStoreApp
//
//  Created by kirill on 17/09/2020.
//  Copyright © 2020 none. All rights reserved.
//

class ReviewsInteractor: ReviewsInteractorInput {

	weak var output: ReviewsInteractorOutput!
	var commentService: CommentsService?

	func fetchComments(
		ProductId productId: Int?,
		completion: @escaping (Comment?) -> Void) {
		guard let productId = productId else { print("\n❗️ 'fetchComments()' productId = nil"); return }
		commentService?.fetchComments(ForProductId: productId) { comment in
			return completion(comment)
		}
	}

	func sendComment(comment: Comment, completion: @escaping (Comment?) -> Void) {
		commentService?.sendComment(comment: comment, completion: { comment in
			completion(comment)
		})
	}
}
