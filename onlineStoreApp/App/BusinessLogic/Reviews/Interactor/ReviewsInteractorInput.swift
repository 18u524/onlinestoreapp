//
//  ReviewsReviewsInteractorInput.swift
//  onlineStoreApp
//
//  Created by kirill on 17/09/2020.
//  Copyright © 2020 none. All rights reserved.
//

import Foundation

protocol ReviewsInteractorInput {
	func fetchComments(ProductId productId: Int?, completion: @escaping (Comment?) -> Void)
	func sendComment(comment: Comment, completion: @escaping (Comment?) -> Void)
}
