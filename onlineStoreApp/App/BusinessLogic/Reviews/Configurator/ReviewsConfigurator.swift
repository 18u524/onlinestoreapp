//
//  ReviewsReviewsConfigurator.swift
//  onlineStoreApp
//
//  Created by kirill on 17/09/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit

class ReviewsModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? ReviewsViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: ReviewsViewController) {

        let router = ReviewsRouter()

        let presenter = ReviewsPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = ReviewsInteractor()
        interactor.output = presenter
		interactor.commentService = CommentsService()

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
