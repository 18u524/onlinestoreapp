//
//  ReviewsReviewsInitializer.swift
//  onlineStoreApp
//
//  Created by kirill on 17/09/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit

class ReviewsModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var reviewsViewController: ReviewsViewController!

    override func awakeFromNib() {

        let configurator = ReviewsModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: reviewsViewController)
    }

}
