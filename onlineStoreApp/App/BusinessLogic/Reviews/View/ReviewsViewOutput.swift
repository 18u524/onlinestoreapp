//
//  ReviewsReviewsViewOutput.swift
//  onlineStoreApp
//
//  Created by kirill on 17/09/2020.
//  Copyright © 2020 none. All rights reserved.
//

protocol ReviewsViewOutput {

    /**
        @author kirill
        Notify presenter that view is ready
    */

	var productId: Int? { get set }

	func sendCommentPressed(commentString: String?)

    func viewIsReady()
}
