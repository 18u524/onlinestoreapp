//
//  ReviewsReviewsViewController.swift
//  onlineStoreApp
//
//  Created by kirill on 17/09/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit

class ReviewsViewController: UIViewController, ReviewsViewInput {

    var output: ReviewsViewOutput!
	@IBOutlet weak var tableView: UITableView!

	@IBOutlet weak var commentField: UITextField!
	@IBAction func sendComment() {
		output.sendCommentPressed(commentString: commentField.text)
	}

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

		self.navigationController?.setNavigationBarHidden(false, animated: true)
		self.setupTableView()
        output.viewIsReady()
    }

	func setupTableView() {
		self.tableView.delegate = self
		self.tableView.dataSource = self

		self.regCollectionCell()
	}

    // MARK: ReviewsViewInput
    func setupInitialState() {
    }
}

extension ReviewsViewController: UITableViewDelegate, UITableViewDataSource {

	private func regCollectionCell() {
		let nib = UINib.init(nibName: "ReviewTableCellTableViewCell", bundle: nil)
		self.tableView.register(nib, forCellReuseIdentifier: "ReviewTableViewCellID")
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 4
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView
			.dequeueReusableCell(withIdentifier: "ReviewTableViewCellID",
								 for: indexPath) as? ReviewTableViewCell else {
			let cell = ReviewTableViewCell()
			return cell
		}

//		guard let product = output.product(atIndex: indexPath) else { return ReviewTableViewCell() }

//		cell.setupCell(product: product)

		return cell
	}

	public func reloadData() {
		self.tableView.reloadData()
	}
}
