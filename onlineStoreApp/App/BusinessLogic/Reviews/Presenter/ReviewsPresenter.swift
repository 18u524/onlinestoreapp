//
//  ReviewsReviewsPresenter.swift
//  onlineStoreApp
//
//  Created by kirill on 17/09/2020.
//  Copyright © 2020 none. All rights reserved.
//
import Foundation

class ReviewsPresenter: ReviewsModuleInput, ReviewsViewOutput, ReviewsInteractorOutput {

    weak var view: ReviewsViewInput!
    var interactor: ReviewsInteractorInput!
    var router: ReviewsRouterInput!

	var productId: Int?

	var comments: [Comment]? {
		didSet {
			view.reloadData()
		}
	}

	func sendCommentPressed(commentString: String?) {
		guard let commentString = commentString else { print("❗️ 'sendCommentPressed()' comment = nil"); return }

		let user = User.shared
		let comment = Comment(avatar: user.avatar, nameLastname: user.username, date: String(Date().timeIntervalSinceNow), text: commentString)

		interactor.sendComment(comment: comment) { [weak self] comment in
			guard let comment = comment else { return }

			ReviewsAddEvent().track()

			self?.comments?.append(comment)
		}
	}

    func viewIsReady() {
		interactor.fetchComments(ProductId: productId) { [weak self] comment in
			guard let comment = comment else { return }
			self?.comments?.append(comment)
		}
    }
}
