//
//  RegisterRegisterInteractorInput.swift
//  onlineStoreApp
//
//  Created by kirill on 20/07/2020.
//  Copyright © 2020 none. All rights reserved.
//

import Foundation

protocol RegisterInteractorInput {
	func postRegRequest(login: String, password: String, email: String)
}
