//
//  RegisterRegisterInteractor.swift
//  onlineStoreApp
//
//  Created by kirill on 20/07/2020.
//  Copyright © 2020 none. All rights reserved.
//

import Alamofire

class RegisterInteractor: RegisterInteractorInput {

	weak var output: RegisterInteractorOutput!

	var registerResult: RegisterResult?

	func postRegRequest(login: String, password: String, email: String) {
		let url = "http://localhost:8080/auth/register"
		let parameters: [String: String] = ["username": login,
											"password": password,
											"email": email]
		AF.request(url, method: .post, parameters:  parameters)
			.responseDecodable(of: RegisterResult.self) { response in
			switch response.result {
			case .success:
				guard let registerResult = response.value else { return }
				self.registerResult = registerResult
			case let .failure(error):
				print(error)
			}
			self.output.regCallBack(registerResult: self.registerResult)
		}
	}
}
