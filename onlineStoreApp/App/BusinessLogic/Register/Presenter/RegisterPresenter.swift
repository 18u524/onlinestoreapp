//
//  RegisterRegisterPresenter.swift
//  onlineStoreApp
//
//  Created by kirill on 20/07/2020.
//  Copyright © 2020 none. All rights reserved.
//

class RegisterPresenter: RegisterModuleInput, RegisterViewOutput, RegisterInteractorOutput {

    weak var view: RegisterViewInput!
    var interactor: RegisterInteractorInput!
    var router: RegisterRouterInput!

	func regButtonPressed(login: String, password: String, email: String) {
		interactor.postRegRequest(login: login, password: password, email: email)
	}

	func regCallBack(registerResult: RegisterResult?) {
		view.animationRegResult(registerResult: registerResult)
	}

    func viewIsReady() {

    }
}
