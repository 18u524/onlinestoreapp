//
//  RegisterRegisterInitializer.swift
//  onlineStoreApp
//
//  Created by kirill on 20/07/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit

class RegisterModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var registerViewController: RegisterViewController!

    override func awakeFromNib() {

        let configurator = RegisterModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: registerViewController)
    }

}
