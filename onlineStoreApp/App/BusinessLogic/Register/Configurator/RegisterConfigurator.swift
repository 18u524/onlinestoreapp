//
//  RegisterRegisterConfigurator.swift
//  onlineStoreApp
//
//  Created by kirill on 20/07/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit

class RegisterModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? RegisterViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: RegisterViewController) {

        let router = RegisterRouter()

        let presenter = RegisterPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = RegisterInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
