//
//  RegisterRegisterViewInput.swift
//  onlineStoreApp
//
//  Created by kirill on 20/07/2020.
//  Copyright © 2020 none. All rights reserved.
//

protocol RegisterViewInput: class {

    /**
        @author kirill
        Setup initial state of the view
    */

	func animationRegResult(registerResult: RegisterResult?)

    func setupInitialState()
}
