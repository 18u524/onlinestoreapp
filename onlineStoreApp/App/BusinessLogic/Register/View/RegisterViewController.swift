//
//  RegisterRegisterViewController.swift
//  onlineStoreApp
//
//  Created by kirill on 20/07/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController, RegisterViewInput {

    var output: RegisterViewOutput!

	@IBOutlet weak var loginTextField: UITextField!
	@IBOutlet weak var passwordTextField: UITextField!
	@IBOutlet weak var emailTextField: UITextField!

	@IBAction func regButtonPressed(_ sender: Any) {
		output.regButtonPressed(login: loginTextField.text ?? "",
								password: passwordTextField.text ?? "",
								email: emailTextField.text ?? "")
	}

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
		loginTextField.text = "kirill"
		passwordTextField.text = "123456"
		emailTextField.text = "email@mail.ru"

		RegistrationEvent().track()
    }

	func animationRegResult(registerResult: RegisterResult?) {
		if registerResult?.error ?? true {
			loginTextField.backgroundColor = #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1)
		}
	}

    // MARK: RegisterViewInput
    func setupInitialState() {
    }
}
