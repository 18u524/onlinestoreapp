//
//  RegisterRegisterViewOutput.swift
//  onlineStoreApp
//
//  Created by kirill on 20/07/2020.
//  Copyright © 2020 none. All rights reserved.
//

protocol RegisterViewOutput {

    /**
        @author kirill
        Notify presenter that view is ready
    */

	func regButtonPressed(login: String, password: String, email: String)

    func viewIsReady()
}
