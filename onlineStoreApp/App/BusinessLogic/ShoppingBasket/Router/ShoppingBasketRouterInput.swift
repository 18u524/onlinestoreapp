//
//  ShoppingBasketShoppingBasketRouterInput.swift
//  onlineStoreApp
//
//  Created by kirill on 20/09/2020.
//  Copyright © 2020 none. All rights reserved.
//

import Foundation

protocol ShoppingBasketRouterInput {
	func goToCheckout()
}
