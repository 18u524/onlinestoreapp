//
//  ShoppingBasketShoppingBasketRouter.swift
//  onlineStoreApp
//
//  Created by kirill on 20/09/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit

class ShoppingBasketRouter: ShoppingBasketRouterInput {
	weak var view: ShoppingBasketViewController?

	func goToCheckout() {
		let viewController = UIStoryboard(name: "Checkout", bundle: nil).instantiateInitialViewController()

		guard let checkoutVC = viewController as? CheckoutViewController else { return }
		view?.navigationController?.pushViewController(checkoutVC, animated: false)
	}
}
