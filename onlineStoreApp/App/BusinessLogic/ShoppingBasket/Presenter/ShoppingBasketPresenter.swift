//
//  ShoppingBasketShoppingBasketPresenter.swift
//  onlineStoreApp
//
//  Created by kirill on 20/09/2020.
//  Copyright © 2020 none. All rights reserved.
//

class ShoppingBasketPresenter: ShoppingBasketModuleInput, ShoppingBasketViewOutput, ShoppingBasketInteractorOutput {

    weak var view: ShoppingBasketViewInput!
    var interactor: ShoppingBasketInteractorInput!
    var router: ShoppingBasketRouterInput!

	var products: [Product] = [] {
		didSet {
			view.reloadData()
		}
	}

	func
	checkoutPressed() {
		router.goToCheckout()
	}

    func viewIsReady() {

    }
}
