//
//  ShoppingBasketShoppingBasketInitializer.swift
//  onlineStoreApp
//
//  Created by kirill on 20/09/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit

class ShoppingBasketModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var shoppingbasketViewController: ShoppingBasketViewController!

    override func awakeFromNib() {

        let configurator = ShoppingBasketModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: shoppingbasketViewController)
    }

}
