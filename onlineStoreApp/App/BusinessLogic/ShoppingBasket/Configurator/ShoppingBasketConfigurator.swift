//
//  ShoppingBasketShoppingBasketConfigurator.swift
//  onlineStoreApp
//
//  Created by kirill on 20/09/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit

class ShoppingBasketModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? ShoppingBasketViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: ShoppingBasketViewController) {

        let router = ShoppingBasketRouter()
		router.view = viewController

        let presenter = ShoppingBasketPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = ShoppingBasketInteractor()
        interactor.output = presenter
		interactor.shoppingBasketService = ShoppingBasketService()

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
