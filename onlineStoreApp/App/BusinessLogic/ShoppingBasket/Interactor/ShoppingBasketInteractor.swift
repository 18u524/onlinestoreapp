//
//  ShoppingBasketShoppingBasketInteractor.swift
//  onlineStoreApp
//
//  Created by kirill on 20/09/2020.
//  Copyright © 2020 none. All rights reserved.
//

class ShoppingBasketInteractor: ShoppingBasketInteractorInput {

    weak var output: ShoppingBasketInteractorOutput!
	var shoppingBasketService: ShoppingBasketServiceProtocol!

	func fetchProducts() {
		shoppingBasketService.fetchProducts { [weak self] products in
			self?.output.products = products
		}
	}
}
