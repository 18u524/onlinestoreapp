//
//  ShoppingBasketTableViewCell.swift
//  onlineStoreApp
//
//  Created by Kirill Titov on 21.09.2020.
//  Copyright © 2020 Кирилл Титов. All rights reserved.
//

import UIKit

class ShoppingBasketTableViewCell: UITableViewCell {
	@IBOutlet weak var productImageView: UIImageView!
	@IBOutlet weak var productCostLabel: UILabel!
	@IBOutlet weak var productTitleLabel: UILabel!
	@IBOutlet weak var productCountLabel: UILabel!

	@IBAction func plusPressed() {

	}
	@IBAction func minusPressed() {

	}

	func setup(image: String, cost: Int, title: String, count: Int = 0) {
		self.productImageView.loadImageUsingUrlString(urlString: image)
		self.productCostLabel.text = String(count*cost)
		self.productTitleLabel.text = title
	}
}
