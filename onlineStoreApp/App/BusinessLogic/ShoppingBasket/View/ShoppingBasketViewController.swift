//
//  ShoppingBasketShoppingBasketViewController.swift
//  onlineStoreApp
//
//  Created by kirill on 20/09/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit

class ShoppingBasketViewController: UIViewController, ShoppingBasketViewInput {

    var output: ShoppingBasketViewOutput!
	@IBOutlet weak var tableView: UITableView!
	@IBAction func checkoutPressed() {
		output.checkoutPressed()
	}

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()

		self.navigationController?.setNavigationBarHidden(false, animated: true)
		self.setupTableView()
    }

    // MARK: ShoppingBasketViewInput
    func setupInitialState() {
    }
}

extension ShoppingBasketViewController: UITableViewDelegate, UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 10//output.products.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

		guard let cell = tableView.dequeueReusableCell(withIdentifier: "ShoppingBasketTableViewCellID", for: indexPath) as? ShoppingBasketTableViewCell else { print("❗️ 'tableView(cellForRowAt) cell = nil'"); return ShoppingBasketTableViewCell() }

//		let product = output.products[indexPath.row]
//		cell.setup(image: product.imageStringUrl, cost: product.cost, title: product.name, count: 1)
		cell.setup(image: "https://static.massimodutti.net/3/photos/2020/I/1/2/p/2773/650/400/2773650400_1_1_16.jpg?t=1595864605371", cost: 100, title: "33")

		return cell
	}

	private func setupTableView() {
		tableView.delegate = self
		tableView.dataSource = self

		self.regCollectionCell()
	}

	func reloadData() {
		self.tableView.reloadData()
	}

	func regCollectionCell() {
		let nib = UINib.init(nibName: "ShoppingBasketTableViewCell", bundle: nil)
		self.tableView.register(nib, forCellReuseIdentifier: "ShoppingBasketTableViewCellID")
	}
}
