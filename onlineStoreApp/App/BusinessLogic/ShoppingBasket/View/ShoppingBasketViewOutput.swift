//
//  ShoppingBasketShoppingBasketViewOutput.swift
//  onlineStoreApp
//
//  Created by kirill on 20/09/2020.
//  Copyright © 2020 none. All rights reserved.
//

protocol ShoppingBasketViewOutput {

    /**
        @author kirill
        Notify presenter that view is ready
    */

	var products: [Product] { get set }

    func viewIsReady()
	func checkoutPressed()
}
