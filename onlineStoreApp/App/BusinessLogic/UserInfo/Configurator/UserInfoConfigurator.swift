//
//  UserInfoUserInfoConfigurator.swift
//  onlineStoreApp
//
//  Created by kirill on 27/08/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit

class UserInfoModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? UserInfoViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: UserInfoViewController) {

        let router = UserInfoRouter()

        let presenter = UserInfoPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = UserInfoInteractor()
        interactor.output = presenter
		interactor.apiManager = UserInfoServices()

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
