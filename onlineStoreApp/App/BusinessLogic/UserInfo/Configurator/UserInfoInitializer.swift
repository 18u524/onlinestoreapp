//
//  UserInfoUserInfoInitializer.swift
//  onlineStoreApp
//
//  Created by kirill on 27/08/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit

class UserInfoModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var userinfoViewController: UserInfoViewController!

    override func awakeFromNib() {

        let configurator = UserInfoModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: userinfoViewController)
    }

}
