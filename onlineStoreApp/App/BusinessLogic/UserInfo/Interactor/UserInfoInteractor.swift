//
//  UserInfoUserInfoInteractor.swift
//  onlineStoreApp
//
//  Created by kirill on 27/08/2020.
//  Copyright © 2020 none. All rights reserved.
//
import Foundation

class UserInfoInteractor: UserInfoInteractorInput {

    weak var output: UserInfoInteractorOutput!
	var apiManager: UserInfoServices!

	public func updateAvatar(data: Data) {
		apiManager.update(Avatar: data) { _ in }
	}
	public func updatePassword(newPassword: String, oldPassword: String) {
		apiManager.update(Password: newPassword,
						  oldPassword: oldPassword) { _ in }
	}
	public func updateUsername(newUsername: String) {
		apiManager.update(Username: newUsername) { _ in }
	}
}
