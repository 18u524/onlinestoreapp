//
//  UserInfoUserInfoInteractorOutput.swift
//  onlineStoreApp
//
//  Created by kirill on 27/08/2020.
//  Copyright © 2020 none. All rights reserved.
//

import Foundation

protocol UserInfoInteractorOutput: class {

	// MARK: Avatar
	func fetchAvatarSuccess(avatar: String)
	func fetchAvatarFailure(errorCode: Int)

	// MARK: Username
	func fetchUsernameSuccess(username: String)
	func fetchUsernameFailure(errorCode: Int)

	// MARK: Password
	func fetchPasswordSuccess(password: String)
	func fetchPasswordFailure(errorCode: Int)
}
