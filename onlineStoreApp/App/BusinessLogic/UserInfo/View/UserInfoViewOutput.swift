//
//  UserInfoUserInfoViewOutput.swift
//  onlineStoreApp
//
//  Created by kirill on 27/08/2020.
//  Copyright © 2020 none. All rights reserved.
//
import Foundation

protocol UserInfoViewOutput {

    /**
        @author kirill
        Notify presenter that view is ready
    */

	func edit(Avatar avatar: Data)
	func edit(Username username: String)
	func edit(Password oldPassword: String, newPassword: String)

    func viewIsReady()
}
