//
//  UserInfoUserInfoViewController.swift
//  onlineStoreApp
//
//  Created by kirill on 27/08/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit

class UserInfoViewController: UIViewController, UserInfoViewInput, showAlert {

    var output: UserInfoViewOutput!

	@IBAction func editAvatar(_ sender: Any) {
	}
	@IBOutlet weak var newUsername: UITextField!

	@IBOutlet weak var oldPassword: UITextField!
	@IBOutlet weak var newPassword: UITextField!
	@IBOutlet weak var newPassword2: UITextField!

	@IBAction func saveChanges(_ sender: Any) {
		if let username = newUsername.text {
			output.edit(Username: username)
		}
		if let oldPassword = oldPassword.text {
			if let newPassword = newPassword.text {
				if newPassword == newPassword2.text {
					output.edit(Password: oldPassword, newPassword: newPassword)
				}
			}
		}
	}

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()

		self.setupInitialState()
		print("")
    }

    // MARK: UserInfoViewInput
    func setupInitialState() {
		// UINavigationController
		self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
}
