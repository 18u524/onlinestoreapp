//
//  UserInfoUserInfoViewInput.swift
//  onlineStoreApp
//
//  Created by kirill on 27/08/2020.
//  Copyright © 2020 none. All rights reserved.
//

protocol UserInfoViewInput: class {

    /**
        @author kirill
        Setup initial state of the view
    */

	func showAlert(message: String?)

    func setupInitialState()
}
