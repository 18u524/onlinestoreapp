//
//  UserInfoUserInfoPresenter.swift
//  onlineStoreApp
//
//  Created by kirill on 27/08/2020.
//  Copyright © 2020 none. All rights reserved.
//

import Foundation

class UserInfoPresenter: UserInfoModuleInput, UserInfoViewOutput, UserInfoInteractorOutput {

    weak var view: UserInfoViewInput!
    var interactor: UserInfoInteractorInput!
    var router: UserInfoRouterInput!

	// MARK: Avatar
	func edit(Avatar avatar: Data) {
		interactor.updateAvatar(data: avatar)
	}
	func fetchAvatarSuccess(avatar: String) {
		User.shared.avatar = avatar
		view.showAlert(message: "Аватар успешно изменен")
	}
	func fetchAvatarFailure(errorCode: Int) {
		view.showAlert(message: "Ошибка: \(errorCode)")
	}

	// MARK: Username
	func edit(Username username: String) {
		interactor.updateUsername(newUsername: username)
	}
	func fetchUsernameSuccess(username: String) {
		User.shared.username = username
		view.showAlert(message: "")
	}
	func fetchUsernameFailure(errorCode: Int) {
		view.showAlert(message: "Ошибка: \(errorCode)")
	}

	// MARK: Password
	func edit(Password oldPassword: String, newPassword: String) {
		interactor.updatePassword(newPassword: newPassword, oldPassword: oldPassword)
	}
	func fetchPasswordSuccess(password: String) {
		view.showAlert(message: "Пароль успешно изменен")
	}
	func fetchPasswordFailure(errorCode: Int) {
		view.showAlert(message: "Ошибка: \(errorCode)")
	}

    func viewIsReady() {

    }
}
