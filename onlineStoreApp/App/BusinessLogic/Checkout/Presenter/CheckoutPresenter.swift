//
//  CheckoutCheckoutPresenter.swift
//  onlineStoreApp
//
//  Created by kirill on 20/09/2020.
//  Copyright © 2020 none. All rights reserved.
//

class CheckoutPresenter: CheckoutModuleInput, CheckoutViewOutput, CheckoutInteractorOutput {

    weak var view: CheckoutViewInput!
    var interactor: CheckoutInteractorInput!
    var router: CheckoutRouterInput!

    func viewIsReady() {

    }
}
