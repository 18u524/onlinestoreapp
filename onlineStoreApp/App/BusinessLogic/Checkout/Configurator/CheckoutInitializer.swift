//
//  CheckoutCheckoutInitializer.swift
//  onlineStoreApp
//
//  Created by kirill on 20/09/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit

class CheckoutModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var checkoutViewController: CheckoutViewController!

    override func awakeFromNib() {

        let configurator = CheckoutModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: checkoutViewController)
    }

}
