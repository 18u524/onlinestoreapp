//
//  CheckoutCheckoutConfigurator.swift
//  onlineStoreApp
//
//  Created by kirill on 20/09/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit

class CheckoutModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? CheckoutViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: CheckoutViewController) {

        let router = CheckoutRouter()

        let presenter = CheckoutPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = CheckoutInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
