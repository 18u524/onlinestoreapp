//
//  CheckoutCheckoutViewInput.swift
//  onlineStoreApp
//
//  Created by kirill on 20/09/2020.
//  Copyright © 2020 none. All rights reserved.
//

protocol CheckoutViewInput: class {

    /**
        @author kirill
        Setup initial state of the view
    */

    func setupInitialState()
}
