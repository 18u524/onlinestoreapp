//
//  CheckoutCheckoutViewController.swift
//  onlineStoreApp
//
//  Created by kirill on 20/09/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit

class CheckoutViewController: UIViewController, CheckoutViewInput {

    var output: CheckoutViewOutput!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()

		self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    // MARK: CheckoutViewInput
    func setupInitialState() {
    }
}
