//
//  CheckoutCheckoutViewOutput.swift
//  onlineStoreApp
//
//  Created by kirill on 20/09/2020.
//  Copyright © 2020 none. All rights reserved.
//

protocol CheckoutViewOutput {

    /**
        @author kirill
        Notify presenter that view is ready
    */

    func viewIsReady()
}
