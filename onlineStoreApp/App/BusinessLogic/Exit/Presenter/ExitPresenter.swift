//
//  ExitExitPresenter.swift
//  onlineStoreApp
//
//  Created by kirill on 22/04/2020.
//  Copyright © 2020 none. All rights reserved.
//

class ExitPresenter: ExitModuleInput, ExitViewOutput, ExitInteractorOutput {

    weak var view: ExitViewInput!
    var interactor: ExitInteractorInput!
    var router: ExitRouterInput!

	func exitButtonPressed() {
		interactor.exitService()
	}

    func viewIsReady() {

    }

	// MARK: - ExitInteractorOutput
	func exitFailure() {
		view.showAlert(message: "Ошибка выхода")
	}

	func exitSucces() {
		router.goToLogIn()
	}
}
