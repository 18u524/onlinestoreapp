//
//  ExitExitConfigurator.swift
//  onlineStoreApp
//
//  Created by kirill on 22/04/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit

class ExitModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? ExitViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: ExitViewController) {

        let router = ExitRouter()

        let presenter = ExitPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = ExitInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
