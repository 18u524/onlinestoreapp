//
//  ExitExitInitializer.swift
//  onlineStoreApp
//
//  Created by kirill on 22/04/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit

class ExitModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var exitViewController: ExitViewController!

    override func awakeFromNib() {

        let configurator = ExitModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: exitViewController)
    }

}
