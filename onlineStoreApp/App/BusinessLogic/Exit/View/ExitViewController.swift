//
//  ExitExitViewController.swift
//  onlineStoreApp
//
//  Created by kirill on 22/04/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit

class ExitViewController: UIViewController, ExitViewInput {

    var output: ExitViewOutput!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }

	@IBAction func exitButton() {
		output.exitButtonPressed()
	}

    // MARK: ExitViewInput
    func setupInitialState() {

    }

	func showAlert(message: String?) {
		let alertC = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
		alertC.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
		self.present(alertC, animated: true, completion: nil)
	}
}
