//
//  ExitExitViewOutput.swift
//  onlineStoreApp
//
//  Created by kirill on 22/04/2020.
//  Copyright © 2020 none. All rights reserved.
//

protocol ExitViewOutput {

    func exitButtonPressed()

    func viewIsReady()
}
