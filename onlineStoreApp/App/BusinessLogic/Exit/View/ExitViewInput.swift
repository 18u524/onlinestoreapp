//
//  ExitExitViewInput.swift
//  onlineStoreApp
//
//  Created by kirill on 22/04/2020.
//  Copyright © 2020 none. All rights reserved.
//

protocol ExitViewInput: class {

	func showAlert(message: String?)

    func setupInitialState()
}
