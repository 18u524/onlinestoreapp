//
//  ExitExitInteractor.swift
//  onlineStoreApp
//
//  Created by kirill on 22/04/2020.
//  Copyright © 2020 none. All rights reserved.
//

class ExitInteractor: ExitInteractorInput {
	func exitService() {

	}

    weak var output: ExitInteractorOutput!

	func exit() {
		// реализовать запуск сервисов

		let bool = true
		if bool {
			output.exitSucces()
		} else {
			output.exitFailure()
		}
	}

}
