//
//  AuthorizationAuthorizationConfigurator.swift
//  onlineStoreApp
//
//  Created by kirill on 18/04/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit

class AuthorizationModuleConfigurator {

	func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

		if let viewController = viewInput as? AuthorizationViewController {
			configure(viewController: viewController)
		}
	}

	private func configure(viewController: AuthorizationViewController) {

		let router = AuthorizationRouter(view: viewController)

		let presenter = AuthorizationPresenter(
			view: viewController,
			router: router)

		let interactor = AuthorizationInteractor(output: presenter)

		presenter.interactor = interactor
		viewController.output = presenter
	}
}
