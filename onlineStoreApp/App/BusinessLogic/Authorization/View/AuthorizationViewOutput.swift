//
//  AuthorizationAuthorizationViewOutput.swift
//  onlineStoreApp
//
//  Created by kirill on 18/04/2020.
//  Copyright © 2020 none. All rights reserved.
//

protocol AuthorizationViewOutput {

	func logInButtonPressed(username: String, password: String)
	func logOnButtonPressed()

	func viewIsReady()
}
