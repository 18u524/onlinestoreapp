//
//  AuthorizationAuthorizationViewController.swift
//  onlineStoreApp
//
//  Created by kirill on 18/04/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit
import FirebaseCrashlytics

class AuthorizationViewController: UIViewController, AuthorizationViewInput, showAlert {

	@IBOutlet weak private var usernameField: UITextField!
	@IBOutlet weak private var passwordField: UITextField!

	@IBAction func logInButtonPressed(_ sender: UIButton) {
		output.logInButtonPressed(username: usernameField.text ?? "", password: passwordField.text ?? "")
	}

	@IBAction func logOnButtonPressed(_ sender: UIButton) {
		output.logOnButtonPressed()
	}

	var output: AuthorizationViewOutput!
	var configurator: AuthorizationModuleConfigurator!

	// MARK: - Life cycle
	override func viewDidLoad() {
		super.viewDidLoad()
		configurator.configureModuleForViewInput(viewInput: self)
		output.viewIsReady()
	}

//	func showAlert(message: String?) {
//		let alertC = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
//		alertC.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//		//		self.present(alertC, animated: true, completion: nil)
//		UIApplication.shared.keyWindow?.rootViewController?.present(alertC, animated: true, completion: nil)
//	}

	// MARK: - AuthorizationViewInput
	func setupInitialState() {

	}
}
