//
//  AuthorizationAuthorizationViewInput.swift
//  onlineStoreApp
//
//  Created by kirill on 18/04/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit

protocol AuthorizationViewInput: class {

	func setupInitialState()
	func showAlert(message: String?)
}
