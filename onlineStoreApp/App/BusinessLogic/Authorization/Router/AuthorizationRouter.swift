//
//  AuthorizationAuthorizationRouter.swift
//  onlineStoreApp
//
//  Created by kirill on 18/04/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit

class AuthorizationRouter: AuthorizationRouterInput {

	private let productListID = "productListID"
	private let registerID = "registerID"

	private weak var view: AuthorizationViewController?

	init(view: AuthorizationViewController) {
		self.view = view
	}

	func goMainMenu() {
		let nav = view?.navigationController
		nav?.setNavigationBarHidden(true, animated: false)

		if let productListVC = UIStoryboard.init(name: "ProductList", bundle: nil)
			.instantiateViewController(identifier: productListID) as? ProductListViewController {

			_ = UINavigationController(rootViewController: productListVC)

			nav?.pushViewController(productListVC, animated: false)
		}
	}

	func goToRegister() {
		let nav = view?.navigationController

		if let registerVC = UIStoryboard.init(name: "Register", bundle: nil)
			.instantiateViewController(identifier: registerID) as? RegisterViewController {
			nav?.pushViewController(registerVC, animated: false)
		}

	}
}
