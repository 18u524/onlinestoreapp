//
//  AuthorizationAuthorizationPresenter.swift
//  onlineStoreApp
//
//  Created by kirill on 18/04/2020.
//  Copyright © 2020 none. All rights reserved.
//

class AuthorizationPresenter: AuthorizationViewOutput, AuthorizationInteractorOutput {

	// MARK: - Properties
	static private let minPasswordLength: UInt = 6

	weak var view: AuthorizationViewInput!
	var interactor: AuthorizationInteractorInput!
	var router: AuthorizationRouterInput!

	// MARK: - init
	init(view: AuthorizationViewInput, router: AuthorizationRouterInput) {
		self.view = view
		self.router = router
	}

	// MARK: - AuthorizationModuleInput

	// MARK: - AuthorizationViewOutput
	func logInButtonPressed(username: String, password: String) {
		interactor.authReq(username: username, password: password)
	}

	func logOnButtonPressed() {
		router.goToRegister()
	}

	func viewIsReady() {

	}

	// MARK: - AuthorizationInteractorOutput
	func authFailure() {
		// todo: передавать сообщением локализованную версию соообщения
		view.showAlert(message: "fail auth")
	}

	func authSuccess() {
		router.goMainMenu()
	}

}
