//
//  AuthorizationAuthorizationInteractorInput.swift
//  onlineStoreApp
//
//  Created by kirill on 18/04/2020.
//  Copyright © 2020 none. All rights reserved.
//

import Foundation

protocol AuthorizationInteractorInput {
	func authReq(username: String, password: String)
}
