//
//  AuthorizationAuthorizationInteractor.swift
//  onlineStoreApp
//
//  Created by kirill on 18/04/2020.
//  Copyright © 2020 none. All rights reserved.
//

import Alamofire

class AuthorizationInteractor: AuthorizationInteractorInput {

	weak var output: AuthorizationInteractorOutput!

	init(output: AuthorizationInteractorOutput) {
		self.output = output
	}

	func authReq(username: String, password: String) {
		NotificationCenter.default
			.addObserver(self,
						 selector: #selector(authReqCallBack(notification:)),
						 name: NSNotification.Name(rawValue: "authCallBack"),
						 object: nil)
		AuthService().request(login: username, password: password)
	}

	@objc func authReqCallBack(notification: Notification) {
		let authResult = notification.object as? AuthResult
		if authResult?.error ?? false {
			LoginFailureEvent().track()
			output.authFailure()
		} else {
			LoginSuccessEvent().track()
			output.authSuccess()
		}
	}
}
