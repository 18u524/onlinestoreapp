//
//  ProductProductPresenter.swift
//  onlineStoreApp
//
//  Created by kirill on 02/09/2020.
//  Copyright © 2020 none. All rights reserved.
//

class ProductPresenter: ProductModuleInput, ProductViewOutput, ProductInteractorOutput {

    weak var view: ProductViewInput!
    var interactor: ProductInteractorInput!
    var router: ProductRouterInput!

	var product: Product?
	var amount: Int = 0 {
		didSet {
			if self.amount >= 10 {
				self.amount = 10
			} else if self.amount <= 1-1 {
				self.amount = 0
			}
			view.update(Counter: self.amount)
			view.setTotalCost(totalCost: amount * (product?.cost ?? 0))
		}
	}

	func plusPressed() {
		self.amount += 1
	}
	func minusPressed() {
		self.amount -= 1
	}

	func extraInfoPressed() {
		router.goToExtraInfo(productId: product?.id)
	}

	func checkoutPressed() {
		guard let product = product else { print("'checkoutPressed()' product = nil"); return }
		router.goToCheckout(product: product, amount: amount)
	}

    func viewIsReady() {
		view.update(Product: product)
		view.update(Counter: amount)
    }
}
