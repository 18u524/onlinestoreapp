//
//  ProductProductConfigurator.swift
//  onlineStoreApp
//
//  Created by kirill on 02/09/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit

class ProductModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? ProductViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: ProductViewController) {

        let router = ProductRouter()
		router.view = viewController

        let presenter = ProductPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = ProductInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
