//
//  ProductProductInitializer.swift
//  onlineStoreApp
//
//  Created by kirill on 02/09/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit

class ProductModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var productViewController: ProductViewController!

    override func awakeFromNib() {

        let configurator = ProductModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: productViewController)
    }

}
