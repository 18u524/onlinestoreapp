//
//  ProductProductRouter.swift
//  onlineStoreApp
//
//  Created by kirill on 02/09/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit

class ProductRouter: ProductRouterInput {
	weak var view: ProductViewController?

	func goToExtraInfo(productId: Int?) {
		guard let productId = productId else { return }
		guard let viewController = UIStoryboard(name: "Reviews", bundle: nil).instantiateInitialViewController() else { return }
		guard let reviewsVC = viewController as? ReviewsViewController else { return }

		reviewsVC.output.productId = productId

		view?.navigationController?.pushViewController(reviewsVC, animated: false)
	}

	func goToCheckout(product: Product, amount: Int) {
		let viewController = UIStoryboard(name: "Checkout", bundle: nil).instantiateInitialViewController()

		guard let checkoutVC = viewController as? CheckoutViewController else { return }
		view?.navigationController?.pushViewController(checkoutVC, animated: false)
	}
}
