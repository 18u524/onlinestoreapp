//
//  ProductProductRouterInput.swift
//  onlineStoreApp
//
//  Created by kirill on 02/09/2020.
//  Copyright © 2020 none. All rights reserved.
//

import Foundation

protocol ProductRouterInput {
	func goToExtraInfo(productId: Int?)
	func goToCheckout(product: Product, amount: Int)
}
