//
//  ProductProductViewInput.swift
//  onlineStoreApp
//
//  Created by kirill on 02/09/2020.
//  Copyright © 2020 none. All rights reserved.
//

protocol ProductViewInput: class {

    /**
        @author kirill
        Setup initial state of the view
    */

	func update(Product product: Product?)
	func update(Counter count: Int)
    func setupInitialState()
	func setTotalCost(totalCost: Int)
}
