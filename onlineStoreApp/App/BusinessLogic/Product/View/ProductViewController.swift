//
//  ProductProductViewController.swift
//  onlineStoreApp
//
//  Created by kirill on 02/09/2020.
//  Copyright © 2020 none. All rights reserved.
//

import UIKit

class ProductViewController: UIViewController, ProductViewInput {

    var output: ProductViewOutput!
	@IBOutlet weak var discountLabel: UILabel!

	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var subtitleLabel: UILabel!
	@IBOutlet weak var costLabel: UILabel!
	@IBOutlet weak var totalCoastLabel: UILabel!

	@IBOutlet weak var imageView: UIImageView!

	@IBOutlet weak var countLabel: UILabel!

	@IBAction func minusPressed(_ sender: Any) {
		output.minusPressed()
	}
	@IBAction func plusPressed(_ sender: Any) {
		output.plusPressed()
	}
	@IBAction func checkoutPressed() {
		output.checkoutPressed()
	}

	@IBAction func extraInfoPressed(_ sender: Any) {
		output.extraInfoPressed()
	}

	func update(Product product: Product?) {
		guard let product = product else { return }
//		self.discountLabel
		self.titleLabel.text = product.name
		self.subtitleLabel.text = nil
		self.costLabel.text = String(product.cost)
		self.imageView.loadImageUsingUrlString(urlString: product.imageStringUrl)
	}

	func update(Counter count: Int) {
		self.countLabel.text = String(count)
	}

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

		ProductOpenedEvent().track()

        output.viewIsReady()
    }
	override func viewWillAppear(_ animated: Bool) {
		self.navigationController?.setNavigationBarHidden(false, animated: true)
	}

	func setTotalCost(totalCost: Int) {
		self.totalCoastLabel.text = String(totalCost)
	}

    // MARK: ProductViewInput
    func setupInitialState() {
    }
}
