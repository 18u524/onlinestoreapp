//
//  ProductProductViewOutput.swift
//  onlineStoreApp
//
//  Created by kirill on 02/09/2020.
//  Copyright © 2020 none. All rights reserved.
//

protocol ProductViewOutput {

    /**
        @author kirill
        Notify presenter that view is ready
    */

    func viewIsReady()

	var product: Product? { get set }

	func extraInfoPressed()
	func checkoutPressed()

	func minusPressed()
	func plusPressed()
}
