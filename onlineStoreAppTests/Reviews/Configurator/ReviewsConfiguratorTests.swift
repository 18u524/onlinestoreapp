//
//  ReviewsReviewsConfiguratorTests.swift
//  onlineStoreApp
//
//  Created by kirill on 17/09/2020.
//  Copyright © 2020 none. All rights reserved.
//

import XCTest

class ReviewsModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

//    func testConfigureModuleForViewController() {

        //given
//        let viewController = ReviewsViewControllerMock()
//        let configurator = ReviewsModuleConfigurator()
//
//        //when
//        configurator.configureModuleForViewInput(viewInput: viewController)
//
//        //then
//        XCTAssertNotNil(viewController.output, "ReviewsViewController is nil after configuration")
//        XCTAssertTrue(viewController.output is ReviewsPresenter, "output is not ReviewsPresenter")
//
//        let presenter: ReviewsPresenter = viewController.output as! ReviewsPresenter
//        XCTAssertNotNil(presenter.view, "view in ReviewsPresenter is nil after configuration")
//        XCTAssertNotNil(presenter.router, "router in ReviewsPresenter is nil after configuration")
//        XCTAssertTrue(presenter.router is ReviewsRouter, "router is not ReviewsRouter")
//
//        let interactor: ReviewsInteractor = presenter.interactor as! ReviewsInteractor
//        XCTAssertNotNil(interactor.output, "output in ReviewsInteractor is nil after configuration")
//    }
//
//    class ReviewsViewControllerMock: ReviewsViewController {
//
//        var setupInitialStateDidCall = false
//
//        override func setupInitialState() {
//            setupInitialStateDidCall = true
//        }
//    }
}
