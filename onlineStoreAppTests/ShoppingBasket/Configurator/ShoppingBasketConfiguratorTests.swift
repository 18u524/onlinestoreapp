//
//  ShoppingBasketShoppingBasketConfiguratorTests.swift
//  onlineStoreApp
//
//  Created by kirill on 20/09/2020.
//  Copyright © 2020 none. All rights reserved.
//

import XCTest

class ShoppingBasketModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

//    func testConfigureModuleForViewController() {
//
//        //given
//        let viewController = ShoppingBasketViewControllerMock()
//        let configurator = ShoppingBasketModuleConfigurator()
//
//        //when
//        configurator.configureModuleForViewInput(viewInput: viewController)
//
//        //then
//        XCTAssertNotNil(viewController.output, "ShoppingBasketViewController is nil after configuration")
//        XCTAssertTrue(viewController.output is ShoppingBasketPresenter, "output is not ShoppingBasketPresenter")
//
//        let presenter: ShoppingBasketPresenter = viewController.output as! ShoppingBasketPresenter
//        XCTAssertNotNil(presenter.view, "view in ShoppingBasketPresenter is nil after configuration")
//        XCTAssertNotNil(presenter.router, "router in ShoppingBasketPresenter is nil after configuration")
//        XCTAssertTrue(presenter.router is ShoppingBasketRouter, "router is not ShoppingBasketRouter")
//
//        let interactor: ShoppingBasketInteractor = presenter.interactor as! ShoppingBasketInteractor
//        XCTAssertNotNil(interactor.output, "output in ShoppingBasketInteractor is nil after configuration")
//    }
//
//    class ShoppingBasketViewControllerMock: ShoppingBasketViewController {
//
//        var setupInitialStateDidCall = false
//
//        override func setupInitialState() {
//            setupInitialStateDidCall = true
//        }
//    }
}
