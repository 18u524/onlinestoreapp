//
//  ExitExitConfiguratorTests.swift
//  onlineStoreApp
//
//  Created by kirill on 22/04/2020.
//  Copyright © 2020 none. All rights reserved.
//

import XCTest
@testable import onlineStoreApp

class ExitModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = ExitViewControllerMock()
        let configurator = ExitModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "ExitViewController is nil after configuration")
        XCTAssertTrue(viewController.output is ExitPresenter, "output is not ExitPresenter")

		if let presenter: ExitPresenter = viewController.output as? ExitPresenter {
			XCTAssertNotNil(presenter.view, "view in ExitPresenter is nil after configuration")
			XCTAssertNotNil(presenter.router, "router in ExitPresenter is nil after configuration")
			XCTAssertTrue(presenter.router is ExitRouter, "router is not ExitRouter")

			if let interactor: ExitInteractor = presenter.interactor as? ExitInteractor {
				XCTAssertNotNil(interactor.output, "output in ExitInteractor is nil after configuration")
			} else {return}
		} else {return}
    }

    class ExitViewControllerMock: ExitViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
