//
//  ProductListProductListConfiguratorTests.swift
//  onlineStoreApp
//
//  Created by kirill on 12/05/2020.
//  Copyright © 2020 none. All rights reserved.
//

import XCTest

class ProductListModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
//        let viewController = ProductListViewControllerMock()
//        let configurator = ProductListModuleConfigurator()
//
//        //when
//        configurator.configureModuleForViewInput(viewInput: viewController)
//
//        //then
//        XCTAssertNotNil(viewController.output, "ProductListViewController is nil after configuration")
//        XCTAssertTrue(viewController.output is ProductListPresenter, "output is not ProductListPresenter")
//
//        let presenter: ProductListPresenter = viewController.output as! ProductListPresenter
//        XCTAssertNotNil(presenter.view, "view in ProductListPresenter is nil after configuration")
//        XCTAssertNotNil(presenter.router, "router in ProductListPresenter is nil after configuration")
//        XCTAssertTrue(presenter.router is ProductListRouter, "router is not ProductListRouter")
//
//        let interactor: ProductListInteractor = presenter.interactor as! ProductListInteractor
//        XCTAssertNotNil(interactor.output, "output in ProductListInteractor is nil after configuration")
//    }
//
//    class ProductListViewControllerMock: ProductListViewController {
//
//        var setupInitialStateDidCall = false
//
//        override func setupInitialState() {
//            setupInitialStateDidCall = true
//        }
    }
}
