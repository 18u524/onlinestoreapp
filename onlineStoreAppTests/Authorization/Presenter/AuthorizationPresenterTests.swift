//
//  AuthorizationAuthorizationPresenterTests.swift
//  onlineStoreApp
//
//  Created by kirill on 18/04/2020.
//  Copyright © 2020 none. All rights reserved.
//

import XCTest
@testable import onlineStoreApp

class AuthorizationPresenterTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

	class MockInteractor: AuthorizationInteractorInput {
		func authReq(username: String, password: String) {

		}
    }

	class MockRouter: AuthorizationRouterInput {
		func goToRegister() {

		}

		func goMainMenu() {

		}

    }

	class MockViewController: AuthorizationViewInput {

		func showAlert(message: String?) {

		}

        func setupInitialState() {

        }
    }
}
