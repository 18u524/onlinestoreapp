//
//  AuthorizationAuthorizationInteractorTests.swift
//  onlineStoreApp
//
//  Created by kirill on 18/04/2020.
//  Copyright © 2020 none. All rights reserved.
//

import XCTest
@testable import onlineStoreApp
import Alamofire

enum ErrorStub: Error {
	case error
}

struct NetworkTestStub: Codable { }

//class ErrorParserStub: AbstractErrorParser {
//	func parse(_ result: Error) -> Error {
//		return ErrorStub.error
//	}
//
//	func parse(response: HTTPURLResponse?, data: Data?, error: Error?) -> Error? {
//		return error
//	}
//}

//class AuthorizationInteractorMock: AuthorizationInteractor {
//	var suspendedAuth = false
//	override func auth(username: String, password: String) {
//		self.suspendedAuth = true
//	}
//}

//class AuthorizationInteractorTests: XCTestCase {
//
//    override func setUp() {
//        super.setUp()
//    }
//
//	let expectation = XCTestExpectation(description: "Wait of response")
//
//	func testNetworkRequest() {
//		// when
//		let url = "https://google.com"
//		let parser = ErrorParserStub()
//
//		// then
//		Alamofire
//			.request(url)
//			.responseCodable(errorParser: parser,
//							 completionHandler: { (result: DataResponse<NetworkTestStub>) in
//								switch result.result {
//								case .failure(let error):
//									XCTFail(description: "")
//								case .success(let data):
//									self.expectation.fulfill()
//								}
//			})
//
//		let failUrl = "https://fail.com"
//
//			// then
//			Alamofire
//				.request(failUrl)
//				.responseCodable(errorParser: parser,
//								 completionHandler: { (result: DataResponse<NetworkTestStub>) in
//									switch result.result {
//									case .failure(let error):
//										self.expectation.fulfill()
//									case .success(let data):
//										XCTFail(description: "")
//
//									}
//				})
//
//		wait(for: [expectation], timeout: 15.0)
//	}
//
//	/// Checking auth method in interactor
//	func testAuth() {
//		// when
//		let username = "username"
//		let password = "password"
//		let view = AuthorizationViewController()
//		let router = AuthorizationRouter(view: view)
//		let preseter = AuthorizationPresenter(view: view, router: router)
//		let interactor = AuthorizationInteractorMock(output: preseter)
//
//		view.output = preseter
//		preseter.interactor = interactor
//
//		// then
//		preseter.logInButtonPressed(username: username, password: password)
//
//		XCTAssert(interactor.suspendedAuth)
//	}
//
//    override func tearDown() {
//        // Put teardown code here. This method is called after the invocation of each test method in the class.
//        super.tearDown()
//    }
//}
