//
//  MainScreenMainScreenConfiguratorTests.swift
//  onlineStoreApp
//
//  Created by kirill on 22/04/2020.
//  Copyright © 2020 none. All rights reserved.
//

import XCTest
@testable import onlineStoreApp

class MainScreenModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

//    func testConfigureModuleForViewController() {
//
//        //given
//        let viewController = MainScreenViewControllerMock()
//        let configurator = ListOfProductsModuleConfigurator()
//
//        //when
//        configurator.configureModuleForViewInput(viewInput: viewController)
//
//        //then
//        XCTAssertNotNil(viewController.output, "MainScreenViewController is nil after configuration")
//        XCTAssertTrue(viewController.output is ListOfProductsPresenter, "output is not MainScreenPresenter")
//
//		if let presenter: ListOfProductsPresenter = viewController.output as? ListOfProductsPresenter {
//			XCTAssertNotNil(presenter.view, "view in MainScreenPresenter is nil after configuration")
//			XCTAssertNotNil(presenter.router, "router in MainScreenPresenter is nil after configuration")
//			XCTAssertTrue(presenter.router is ListOfProductsRouter, "router is not MainScreenRouter")
//
//			if let interactor: ListOfProductsInteractor = presenter.interactor as? ListOfProductsInteractor {
//				XCTAssertNotNil(interactor.output, "output in MainScreenInteractor is nil after configuration")
//			} else {return}
//		} else {return}
//    }
//
//    class MainScreenViewControllerMock: ListOfProductsViewController {
//
//        var setupInitialStateDidCall = false
//
//        override func setupInitialState() {
//            setupInitialStateDidCall = true
//        }
//    }
}
